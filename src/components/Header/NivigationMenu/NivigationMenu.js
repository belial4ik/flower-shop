import React from 'react';
import { NavLink } from 'react-router-dom'
import classes from './NivigationMenu.module.css'

const NivigationMenu = () => {
        return (
            <div className={classes.NivigationMenu}>
                <ul className={classes.NavigationList}>
                        <NavLink exact activeClassName={classes.activeLink} to="/" >
                            Home
                        </NavLink>
                        <NavLink exact activeClassName={classes.activeLink} to="/flowers" >
                            Flowers
                        </NavLink>
                        <NavLink exact activeClassName={classes.activeLink} to="/contacts" >
                            Contacts
                        </NavLink>
                        <NavLink exact activeClassName={classes.activeLink} to="/comments" >
                            Comments
                        </NavLink>
                </ul>
            </div>
        );
}

export default NivigationMenu;