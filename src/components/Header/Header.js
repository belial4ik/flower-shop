import React from 'react';
import classes from './Header.module.css'
import { FaFacebookSquare, FaTwitter, FaInstagram } from "react-icons/fa";
import { IconContext } from "react-icons";

const Header = () => {
    return (
        <div className={classes.Header}>
            <div className={classes.HeaderContacts}>
                <p>+38(063)562-01-04</p>
                <p>belial.selimov@gmail.com</p>
            </div>
            <h1 className={classes.Title}>F<span>lowe</span>r store</h1>
            <div className={classes.HeaderIcons}>
                    <IconContext.Provider value={{ size: "2rem" }}>
                        <div>
                            <a style={{color: 'blue'}} href="#"><FaFacebookSquare /></a>
                        </div>
                        <div>
                            <a style={{color: 'lightblue'}} href="#"><FaTwitter /></a>
                        </div>
                        <div>
                            <a style={{color: 'pink'}} href="#"><FaInstagram /></a>
                        </div>
                    </IconContext.Provider>
            </div>
        </div>
    );
};
export default Header;