import React, {Fragment} from 'react';
import Header from './Header'
import NavigationMenu from './NivigationMenu/NivigationMenu'

const HeaderContainer = () => {
    return (
        <Fragment>
            <Header />
            <NavigationMenu />
        </Fragment>
    );
}
export default HeaderContainer;