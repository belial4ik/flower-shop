import React from 'react';
import classes from './HomePage.module.css'

const Main = () => {
    return (
        <div className={classes.Main}>
            <div className={classes.Container}>
                <h3>This web site introduce with my developer skills</h3>
            </div>
        </div>
    );
};

export default Main;