import React from 'react';
import classes from './CategoriesMenu.module.css';

const categories = ['Хризантема','Эустома','Мультифлора','Тюльпан']

const CategoriesMenu = () => {
    return (
        <div className={classes.CategoriesMenu}>
            {/* <div className={classes.TitleItem}>
                <h3>Categories Menu</h3>
            </div> */}
            <ul>
                <li>Хризантема</li>
                <li>Эустома</li>
                <li>Тюльпан</li>
                <li>Мультифлора</li>
            </ul>
        </div>
    )
};

export default CategoriesMenu;