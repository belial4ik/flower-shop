import React from 'react';
import CategoriesMenu from './components/CategoriesMenu/CategoriesMenu';
import Categories from './components/Categories/Flowers';
import classes from './CategoriesContainer.module.css'

const CategoriesContainer =() => {
    return (
        <div className={classes.Container}>
            <CategoriesMenu />
            <Categories />
        </div>
    )
};

export default CategoriesContainer;