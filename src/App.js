import React from 'react';
import { Route } from 'react-router-dom';
import './App.css';
import HeaderContainer from './components/Header/HeaderContainer';
import Main from './pages/HomePage/HomePage';
import CategoriesContainer from './pages/Categories/CategoriesContainer';
import Contacts from './pages/Contacts/Contacts';
import Comments from './pages/Comments/Comments';

function App() {
  return (
    <div className="App">
      <HeaderContainer />
      <Route exact path="/" component={Main} />
      <Route exact path="/flowers" component={CategoriesContainer} />
      <Route exact path="/contacts" component={Contacts} />
      <Route exact path="/comments" component={Comments} />
    </div>
  );
}

export default App;
